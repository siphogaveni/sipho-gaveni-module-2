void main(List<String> args) {
  MtnApp winner = new MtnApp('appName', 'category', 'developer', 00);
  winner._appName = 'Ambani Africa';
  winner._category = 'Educational solutions';
  winner._developer = 'Ambani';
  winner._year = 2021;
  var message = winner.message();
  print(message);
}

class MtnApp {
  String _appName = "";
  String _category = "";
  String _developer = "";
  int _year = 0;

  MtnApp(String appName, String category, String developer, int year) {
    this._appName = appName;
    this._category = category;
    this._developer = developer;
    this._year = year;
  }

  String upperCase(String name) {
    name = this._appName.toUpperCase();
    return name;
  }

  String message() {
    String text =
        '${upperCase(_appName)} was the best ${_category} in ${_year},and was developed by${_developer}';
    return text;
  }
}
